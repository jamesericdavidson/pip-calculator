﻿/*
	Copyright 2018-2020 James Davidson

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
 
// Define pips required for each tier.

const wood = 100;
const bronze = 120;
const silver = 175;
const gold = 200;
const platinum = 225;
const mithril = 300;
const diamond = 330;

var tierArray = [
    wood,
    bronze,
    silver,
    gold,
    platinum,
    mithril,
    diamond
];

function getResult() {
    if (isNumberAllowed() === true) {
        var pips = txtPips.value * 1;
        calculateResult(pips);
    } else {
        window.alert("Please enter a number between 3 and 19.");
        clearPage();
    }
}

function onChange() {
    // When selDivision fires an onchange event and txtPips
    // has no value, it will populate undefined.
    if (txtPips.value === "") {
        return;
    }
}

function calculateResult(pips) {
    var hours = 0;
    var minutes = 0;
    var totalTime = 0;

    var endTier = getEndTier();
    clearOldResults(endTier);

    for (var i = 0; i < endTier; i++) {
        var timeToTier = calculateTime(tierArray[i], pips);
        hours = getHours(timeToTier);
        minutes = getMinutes(timeToTier);
        writeResultToPage(hours, minutes, i);
        totalTime += timeToTier;
    }

    // Calculate and display the total.
    hours = getHours(totalTime);
    minutes = getMinutes(totalTime);
    calculateTotalTime(hours, minutes);
}

function calculateTotalTime(hours, minutes) {
    var total = determineTextContent(hours, minutes);
    document.getElementById('parTotal').textContent = total;
}

function writeResultToPage(hours, minutes, index) {
    var paragraphText = determineTextContent(hours, minutes);
    var parId = getParagraphId(index);
    document.getElementById(parId).textContent = paragraphText;
}

function determineTextContent(hours, minutes) {
    var caseSwitch = getCaseSwitch(hours, minutes);

    switch (caseSwitch) {
        case "minutes":
            return minutes + " minutes";
            break;
        case "hours":
            return hours + " hours";
            break;
        case "hour":
            return hours + " hour";
            break;
        case "hourAndMinute":
            return hours = " hour " + minutes + " minute";
            break;
        case "hourAndMinutes":
            return hours + " hour " + minutes + " minutes";
            break;
        case "hoursAndMinutes":
            return hours + " hours " + minutes + " minutes";
            break;
    }
}

function getCaseSwitch(hours, minutes) {
    var caseSwitch;

    if (hours === 0 && minutes > 1) {
        caseSwitch = "minutes";
    } else if (hours > 1 && minutes === 0) {
        caseSwitch = "hours";
    } else if (hours === 1 && minutes === 0) {
        caseSwitch = "hour";
    } else if (hours === 1 && minutes === 1) {
        caseSwitch = "hourAndMinute";
    } else if (hours === 1 && minutes > 1) {
        caseSwitch = "hourAndMinutes";
    } else if (hours > 1 && minutes > 1) {
        caseSwitch = "hoursAndMinutes";
    }

    return caseSwitch;
}

function clearOldResults(endTier) {
    for (var i = 6; i >= endTier; i--) {
        var parId = getParagraphId(i);
        document.getElementById(parId).textContent = "";
    }
}

function clearPage() {
    txtPips.value = "";
    const allTiers = 0;
    clearOldResults(allTiers);
    parTotal.textContent = "";
}

function getParagraphId(index) {
    var parArray = [
        "parWood",
        "parBronze",
        "parSilver",
        "parGold",
        "parPlatinum",
        "parMithril",
        "parDiamond"
    ];

    return parArray[index];
}

function calculateTime(tier, pips) {
    const tick = 5;
    var time = tier / pips * tick;
    return time;
}

function getHours(time) {
    var hours = Math.floor(time / 60);
    return hours;
}

function getMinutes(time) {
    var minutes = Math.round(time % 60);
    return minutes;
}

function getEndTier() {
    return selDivision.value === "" ? tierArray.length : selDivision.value;
}

function isNumberAllowed() {
    return txtPips.value >= 3 && txtPips.value <= 19 ? true : false;
}

function isNumberKey(evt) {
    // Source: https://stackoverflow.com/questions/9732455/how-to-allow-only-integers-in-a-textbox/9732876
    var charCode = evt.which ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
