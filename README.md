# Guild Wars 2 Pip Calculator *(discontinued)*

*Estimate the time it will take complete divisions in World vs World*

| [Try Pip Calculator from the browser](https://jamesdavidson.xyz/demos/pip-calculator/) |
| -------------------------------------------------------------------------------------- |
| [![Screenshot demonstration of the Pip Calculator](demo.png)](https://jamesdavidson.xyz/demos/pip-calculator/) |

This project is outdated and incomplete. Use the [WvW Pip Calculator for Guild Wars 2](https://gw2.limitlessfx.com/wvw/pips.php) instead.

---

* Totalling each divisions 'time to complete' does not match the total given by the calculator

    This is due to the way `totalTime` is calculated:

    1. Each division (referred to as a `tier`) is represented with floored hours and rounded minutes
    2. `totalTime` adds the decimal value of `timeToTier` to itself every time `timeToTier` is calculated
    3. Once the loop is finished, `totalTime` is floored and rounded, resulting in a total time that is greater than the sum of its parts
